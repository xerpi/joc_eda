#include "Player.hh"
#include <list>

using namespace std;

/**
* Escriu el nom * del teu jugador i guarda 
* aquest fitxer amb el nom AI*.cc
*/
#define PLAYER_NAME Serenity


/**
* Podeu declarar constants aquí
*/

#define cdbg cerr << "DEBUG: "
const int deltaX[8] = { -1, 1, 0,  0, -1, 1,  1, -1 };
const int deltaY[8] = {  0, 0, 1, -1,  1, 1, -1, -1 };
const Posicio posMenys1Menys1 = Posicio(-1, -1);

struct PLAYER_NAME : public Player {

	/**
	* Factory: retorna una nova instància d'aquesta classe.
	* No toqueu aquesta funció.
	*/
	static Player* factory ()
	{
		return new PLAYER_NAME;
	}

	/**
	* Els atributs dels vostres jugadors es poden definir aquí.
	*/
	typedef vector<Posicio> FilaPosicio;
	typedef vector<FilaPosicio> MapaPosicio;
	typedef queue<Posicio> Path;
	
	static bool destiFunc_Posts(PLAYER_NAME &player, const Posicio &inicial, const Posicio &actual, void *usrdata)
	{
		//Mirem si hi ha un post
		int equip_post = player.de_qui_post(actual.x, actual.y);
		//Mirem que no sigui de ningu o no sigui nostre
		return ((equip_post == -1) or ((equip_post > 0) and (equip_post != player.qui_soc())));
	}
	
	static bool destiFunc_Posicio(PLAYER_NAME &player, const Posicio &inicial, const Posicio &actual, void *usrdata)
	{
		Posicio desti = *(Posicio *)usrdata;
		return not (desti != actual);
	}
	
	bool buscaCami(const Posicio &initialPos, bool (*destiFunc)(PLAYER_NAME &player, const Posicio &inicial, const Posicio &actual, void *usrdata), void *usrdata, int rows, int cols, Path &finalPath)
	{
		MapaPosicio posAnterior(rows, FilaPosicio(cols, {-1, -1}));
		bool trobat = false;
		Posicio front;
		queue<Posicio> q;
		q.push(initialPos);
		
		while (not q.empty() and not trobat) {
			front = q.front();
			q.pop();
			//Mirem si estem on volem arribar...
			if (destiFunc(*this, initialPos, front, usrdata)) {
				trobat = true;
			} else {
				//Mirem al voltant
				for (int i = 0; i < 8; i++) {
					int x = front.x + deltaX[i];
					int y = front.y + deltaY[i];
					//Mirem que no ens passem del mapa
					if ((x > 0) and (x < rows-1) and (y > 0) and (y < cols-1)) {
						//No visitat <=> {-1, -1}
						if (not (posAnterior[x][y] != posMenys1Menys1)) {
						//Mirem que no hi hagi foc
							int foc = temps_foc(x, y);
							if (foc == 0) {
								int tipus_zona = que(x, y);
								//Mirem que poguem passar
								if (tipus_zona != AIGUA and tipus_zona != MUNTANYA) {
									posAnterior[x][y] = front;
									q.push({x, y});
								}
							}
						}
					}
				}
			}
		}
		if (trobat) {
			//FER EL CAMI!!!!
			while (front != initialPos) {
				finalPath.push(front);
				front = posAnterior[front.x][front.y];
			}
			return true;
		}
		return false;
	}

	/**
	* Mètode play.
	* 
	* Aquest mètode serà invocat una vegada cada torn.
	*/     
	virtual void play ()
	{
		int equip = qui_soc();
		
		//Soldats
		vector<int> s = soldats(equip);
		for (auto id : s) {
			Info i = dades(id);
			bool mogut = false;
			//Mirem si tenim un enemic al costat
			for (int j = 0; j < 8 and not mogut; j++) {
				int x = i.pos.x + deltaX[j];
				int y = i.pos.y + deltaY[j];
				int quin = quin_soldat(x, y);
				//Hem trobat un soldat, mirem de quin equip es
				if (quin > 0) {
					Info dades_trobat = dades(quin);
					//Si es un enemic, lluitem
					if (dades_trobat.equip != equip) {
						ordena_soldat(id, x, y);
						mogut = true;
					}
				}
			}
			//Si no tenim cap enemic al costat, ens movem cap al post
			if (not mogut) {
				Path path;
				/* AGRESIU */
				if (torns_restants() < 30) {
					//Si estem en un post, ens quedem dins
					if (valor_post(i.pos.x, i.pos.y) <= 0) {
						buscaCami(i.pos, destiFunc_Posts, NULL, MAX, MAX, path);
					}
				} else { /* DEFENSIU */
					Posicio pos(0,0);
					if (equip == 1)
						pos = Posicio(1, MAX/2 + 10);
					else if (equip == 2)
						pos = Posicio(1, MAX/2 - 10);
					else if (equip == 3)
						pos = Posicio(MAX-2, MAX/2 - 10);
					else if (equip == 4)
						pos = Posicio(MAX-2, MAX/2 + 10);
					buscaCami(i.pos, destiFunc_Posicio, (void*)&pos, MAX, MAX, path);
				}
				if (path.size() > 0) {
					ordena_soldat(id, path.back().x, path.back().y);
				}
			}
		}
		
		//Helicòpters
		vector<int> h = helis(equip);
		for (auto heli: h) {
			Info info = dades(heli);
			int x = info.pos.x;
			int y = info.pos.y;
			int suma = 0;
			for (int i = -ABAST; i <= ABAST; ++i) {
				for (int j = -ABAST; j <= ABAST; ++j) {
					int xx = x + i;
					int yy = y + j;
					if (xx >= 0 and xx < MAX and yy >= 0 and yy < MAX) {
						int id2 = quin_soldat(xx, yy);
						if (id2 and info.equip != equip) ++suma;
					}
				}
			}
			
			// Si tenim 3 o més enemics a sota, intentem llençar napalm.
			if (suma >= 2 and info.napalm == 0) {
				ordena_helicopter(heli, NAPALM);
			}
			
			if (info.paraca.size() > 0) {
				if (que(info.pos.x, info.pos.y) != AIGUA)
					ordena_paracaigudista(info.pos.x, info.pos.y);
			}
		}
	}


};


/**
* No toqueu aquesta línia.
*/
RegisterPlayer(PLAYER_NAME);

